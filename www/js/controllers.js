var app = angular.module('tedrssapp.controllers', []);

app.controller('FeedCtrl', function ($scope, FeedService, $ionicLoading) {
	console.log("Loading FeedCtrl");

	$ionicLoading.show({template:"Loading feed..."})
	$scope.feed = FeedService;
	$scope.feed.loadFeed().then(function(){
		$ionicLoading.hide();
	});

	$scope.doRefresh = function () {
		$scope.feed.loadFeed().then(function () {
			$scope.$broadcast('scroll.refreshComplete');
		});
	};



});

app.controller('PostCtrl', function ($scope, $stateParams, FeedService, $window, $cordovaSocialSharing) {
	console.log("Loading PostCtrl");

	$scope.postId = $stateParams.id;
	$scope.post = FeedService.getEntry($scope.postId);

	$scope.share = function () {
		console.debug("Sharing post");

		// Share via native share sheet
		$cordovaSocialSharing
		    .share(
		    	"Here is a TED video I like", 
		    	$scope.post.title, 
		    	$scope.post.thumbnail, 
		    	$scope.post.link); 


	};

	$scope.playLocal = function () {
		console.debug("Read more post");
		$window.open($scope.post.video, "_system", "location=yes");
	}

	$scope.readMore = function () {
		console.debug("Read more post");
		$window.open($scope.post.link, "_system", "location=yes");
	};

});
