# Intro #

I'm a big fan of the TED talks !  Who isn't ?
The official app only supports the Chromecast protocol.  At home, I've only got DLNA.  So this means I just can't watch my favorite talks on my TV.  I could use youtube, but it isn't ideal because when you search for TED talks, you get all kind of unrelated results.

So, that's the value that TEDAir adds:  being able to play a TED video from my mobile device to any device supporting the following protocols:
DLNA, DIAL, SSAP, ECG, AirPlay, Chromecast, UDAP, and webOS second screen protocol.

To do so, i'm using Ionic Framework (cordova, gulp, SASS, AngularJS, command line tools similar to yeoman) in combination with LG's connect SDK which abstracts the protocols layer.

# ROADMAP #

 - Integrate ConnectSDK (see experimental project at https://bitbucket.org/b00stup/helloconnect)

 - Add a button to cast videos

## Current State ##
 
 - Can resume integration of ConnectSDK (LG finally paid for their domain name, so their website is up again : www.connectsdk.com !)
 
 - the images returned by the RSS feed are way to large.  The solution is a back end one, so I am not planning on handling this portion, although **these large images do make the application lag**.